
# Entity: maxmin_top 
- **File**: maxmin_top.vhd

## Diagram
![Diagram](maxmin_top.svg "Diagram")
## Ports

| Port name     | Direction | Type                         | Description |
| ------------- | --------- | ---------------------------- | ----------- |
| clk           | in        | std_logic                    |             |
| i_en          | in        | std_logic                    |             |
| i_x0_data     | in        | std_logic_vector(4 downto 0) |             |
| i_x1_data     | in        | std_logic_vector(4 downto 0) |             |
| i_x2_data     | in        | std_logic_vector(4 downto 0) |             |
| i_x3_data     | in        | std_logic_vector(4 downto 0) |             |
| o_neuron0_out | out       | std_logic_vector(8 downto 0) |             |
| o_neuron1_out | out       | std_logic_vector(8 downto 0) |             |
| o_neuron2_out | out       | std_logic_vector(8 downto 0) |             |
| o_done        | out       | std_logic                    |             |

## Signals

| Name       | Type                                               | Description |
| ---------- | -------------------------------------------------- | ----------- |
| maxmin_i_x | pkg_xi_array_t                                     |             |
| maxmin_i_w | pkg_w_array_t                                      |             |
| lfsr1_seed | std_logic_vector (PKG_BIT_PRECISSION - 1 downto 0) |             |
| lfsr2_seed | std_logic_vector (PKG_BIT_PRECISSION - 1 downto 0) |             |
| o_result   | pkg_y_slv_array_t                                  |             |
| rst        | std_logic                                          |             |

## Instantiations

- maxmin_inst: maxmin_net
