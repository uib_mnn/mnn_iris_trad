LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.maxmin_net_pkg.all;
USE work.MyTypes.all;

---------------------------------------------------------------
-- Version		Authors				Date				Changes
---------------------------------------------------------------
-- 0.1 			CCFF             27/10/2021          	Initial

--=============================================================

ENTITY neurona_maxmin IS
	generic (				
		-- NEURON_N_INPUTS: integer:=29; -- no needed as pkg_xi_array_t has the number of inputs.
		MAXMIN : integer := PKG_MAXMIN_NEURON_TYPE_MIN
	);
	PORT
	(
		-- Inputs
		clock : IN STD_LOGIC; 
		reset : IN STD_LOGIC;		
		
		i_lfsr : 		IN signed (PKG_BIT_PRECISSION-1 DOWNTO 0);
		i_x : 			IN pkg_xi_array_t;
		i_w : 			IN pkg_xi_array_t;
	
		--Outputs
		o_y_sc : 		OUT STD_LOGIC		
	);
	
END neurona_maxmin;

ARCHITECTURE arch OF neurona_maxmin IS
	
	signal sum_array : pkg_xi_array_t;
	signal sum_sc : std_logic_vector(PKG_MAXMIN_N_INPUTS-1 downto 0):= (others=>'0');

BEGIN

	------------------------------------------------
	-- Addition Layer
	------------------------------------------------
	process (i_x, i_w)
		variable TMP_SUM : signed (PKG_INPUT_WIDTH_BITS downto 0):=(others=>'0');
	begin
		
		for i in 0 to PKG_MAXMIN_N_INPUTS-1 loop
			TMP_SUM := resize(i_x(i), TMP_SUM'length) + resize(i_w(i), TMP_SUM'length) ;
			-- CLAMP
			if TMP_SUM > (2**(PKG_BIT_PRECISSION-1)-1) then
				TMP_SUM := to_signed(2**(PKG_BIT_PRECISSION-1)-1 ,TMP_SUM'length);
			end if;
			if TMP_SUM < -(2**(PKG_BIT_PRECISSION-1)-1) then
				TMP_SUM := to_signed(-1*(2**(PKG_BIT_PRECISSION-1)-1) ,TMP_SUM'length);
			end if;
			sum_array(i) <= TMP_SUM(PKG_BIT_PRECISSION-1 DOWNTO 0);
		end loop;
		
	end process;

	------------------------------------------------
	-- SUM conversion to SC
	------------------------------------------------
	b2p_sum_inst: FOR index IN 0 TO PKG_MAXMIN_N_INPUTS-1 GENERATE 
		sum_sc(index) <= '1' when sum_array(index) >= i_lfsr else '0';
	END GENERATE;

	------------------------------------------------
	-- MAX or MIN Output SC 
	------------------------------------------------
	gen_max_output: if MAXMIN = PKG_MAXMIN_NEURON_TYPE_MAX generate
		process (sum_sc)
			variable TMP : std_logic;
		begin
			TMP := '0';
			for I in 0 to PKG_MAXMIN_N_INPUTS-1 loop
				TMP := TMP or sum_sc(I);
			end loop;
			o_y_sc <= TMP;
		end process;		
	end generate gen_max_output;

	gen_min_output: if MAXMIN = PKG_MAXMIN_NEURON_TYPE_MIN generate
		process (sum_sc)
			variable TMP : std_logic;
		begin
			TMP := '1';
			for I in 0 to PKG_MAXMIN_N_INPUTS-1 loop
				TMP := TMP and sum_sc(I);
			end loop;
			o_y_sc <= TMP;
		end process;		
	end generate gen_min_output;
	

END arch;
