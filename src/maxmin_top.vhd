
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use work.MyTypes.all;
use work.maxmin_net_pkg.all;
--============================================================
--
-- 
---------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 0.1 			CCFF            
--=============================================================
entity maxmin_top is
  port
  (
    -- Inputs
    clk  : in std_logic;
    i_en : in std_logic;

    -- X input		
    i_x0_data : in std_logic_vector(4 downto 0);
    i_x1_data : in std_logic_vector(4 downto 0);
    i_x2_data : in std_logic_vector(4 downto 0);
    i_x3_data : in std_logic_vector(4 downto 0);

    --Outputs
    o_neuron0_out : out std_logic_vector(8 downto 0);
    o_neuron1_out : out std_logic_vector(8 downto 0);
    o_neuron2_out : out std_logic_vector(8 downto 0);

    o_done : out std_logic

  );

end maxmin_top;

architecture arch of maxmin_top is

  signal maxmin_i_x             : pkg_xi_array_t                                     := (others => (others => '0'));
  signal maxmin_i_w             : pkg_w_array_t                                      := (others => (others => '0'));
  signal lfsr1_seed, lfsr2_seed : std_logic_vector (PKG_BIT_PRECISSION - 1 downto 0) := (others => '0');
  signal o_result               : pkg_y_slv_array_t                                  := (others => (others => '0'));
  signal rst                    : std_logic                                          := '0';

  component maxmin_net is
    port
    (
      clock, reset, enable   : in std_logic;
      lfsr1_seed, lfsr2_seed : in std_logic_vector (PKG_BIT_PRECISSION - 1 downto 0);
      mlpIn_x                : in pkg_xi_array_t;
      mlpIn_w                : in pkg_w_array_t;
      mlpOut_y               : out pkg_y_slv_array_t;
      mlpOut_done            : out std_logic;
      mlpOut_next            : out std_logic
    );
  end component;

begin

  -- Instantation 
  maxmin_inst : maxmin_net
  port map
  (
    clk, rst, i_en,
    lfsr1_seed,
    lfsr2_seed,
    maxmin_i_x,
    maxmin_i_w,
    o_result,
    o_done,
    open);

  rst <= not(i_en);

  -- X Register Bank
  maxmin_i_x(0) <= signed(i_x0_data);
  maxmin_i_x(1) <= signed(i_x1_data);
  maxmin_i_x(2) <= signed(i_x2_data);
  maxmin_i_x(3) <= signed(i_x3_data);

  -- Outputs
  o_neuron0_out <= o_result(0);
  o_neuron1_out <= o_result(1);
  o_neuron2_out <= o_result(2);
  -- Hardwired values
  lfsr1_seed <= std_logic_vector(to_signed(-1, PKG_BIT_PRECISSION));
  lfsr2_seed <= std_logic_vector(to_signed(-6, PKG_BIT_PRECISSION));
  -- Weights
  maxmin_i_w(0)  <= to_signed(2, PKG_BIT_PRECISSION);
  maxmin_i_w(1)  <= to_signed(-6, PKG_BIT_PRECISSION);
  maxmin_i_w(2)  <= to_signed(15, PKG_BIT_PRECISSION);
  maxmin_i_w(3)  <= to_signed(7, PKG_BIT_PRECISSION);
  maxmin_i_w(4)  <= to_signed(-7, PKG_BIT_PRECISSION);
  maxmin_i_w(5)  <= to_signed(-12, PKG_BIT_PRECISSION);
  maxmin_i_w(6)  <= to_signed(1, PKG_BIT_PRECISSION);
  maxmin_i_w(7)  <= to_signed(3, PKG_BIT_PRECISSION);
  maxmin_i_w(8)  <= to_signed(3, PKG_BIT_PRECISSION);
  maxmin_i_w(9)  <= to_signed(8, PKG_BIT_PRECISSION);
  maxmin_i_w(10) <= to_signed(-4, PKG_BIT_PRECISSION);
  maxmin_i_w(11) <= to_signed(-10, PKG_BIT_PRECISSION);
  maxmin_i_w(12) <= to_signed(8, PKG_BIT_PRECISSION);
  maxmin_i_w(13) <= to_signed(10, PKG_BIT_PRECISSION);
  maxmin_i_w(14) <= to_signed(-5, PKG_BIT_PRECISSION);
  maxmin_i_w(15) <= to_signed(4, PKG_BIT_PRECISSION);
  -- MLP LAYERS
  maxmin_i_w(16) <= to_signed(-12, PKG_BIT_PRECISSION);
  maxmin_i_w(17) <= to_signed(-15, PKG_BIT_PRECISSION);
  maxmin_i_w(18) <= to_signed(-11, PKG_BIT_PRECISSION);
  maxmin_i_w(19) <= to_signed(-11, PKG_BIT_PRECISSION);
  maxmin_i_w(20) <= to_signed(0, PKG_BIT_PRECISSION);
  maxmin_i_w(21) <= to_signed(5, PKG_BIT_PRECISSION);
  maxmin_i_w(22) <= to_signed(-4, PKG_BIT_PRECISSION);
  maxmin_i_w(23) <= to_signed(-5, PKG_BIT_PRECISSION);
  maxmin_i_w(24) <= to_signed(0, PKG_BIT_PRECISSION);
  maxmin_i_w(25) <= to_signed(0, PKG_BIT_PRECISSION);
  maxmin_i_w(26) <= to_signed(7, PKG_BIT_PRECISSION);
  maxmin_i_w(27) <= to_signed(14, PKG_BIT_PRECISSION);
  maxmin_i_w(28) <= to_signed(12, PKG_BIT_PRECISSION);
  maxmin_i_w(29) <= to_signed(14, PKG_BIT_PRECISSION);
  maxmin_i_w(30) <= to_signed(0, PKG_BIT_PRECISSION);

end arch;