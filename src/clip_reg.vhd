-- autogenerate, clip_reg v1

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
--use work.MyTypes.all;

ENTITY clip_reg IS 
    GENERIC(
        INPUT_FULL_SCALE_WIDTH : INTEGER := 4;
		OUTPUT_WIDTH : INTEGER := 8;
        APC_SCALE_WIDTH : INTEGER := 5
    );
    PORT(
        -- inputs
		  clk : in std_logic;
		  en : in std_logic;
		--
        -- 
        --  Formula:
        --          apc_scaled_down = i_full_range/2^(i_apc_scale_idx)
        --
        -- Ejmplo de tabla en el caso que OUTPUT_WIDTH = 8
        -----------------------------------------
        -- i_apc_scale_idx      o_output_scaled
        -- --------------------------------------
        -- 0                    i_full_range[7 dwnto 0]
        -- 1                    i_full_range[8 dwnto 1]
        -- 2                    i_full_range[9 dwnto 2]

          i_apc_scale_idx		: in std_logic_vector(APC_SCALE_WIDTH-1 downto 0);
          i_full_range 		: in signed(INPUT_FULL_SCALE_WIDTH-1 downto 0); 
		  
        -- outputs
          o_output_scaled		: out signed(OUTPUT_WIDTH-1 downto 0)
   );
END clip_reg;

ARCHITECTURE Behavioral OF clip_reg IS
    
	signal apc_scale_int : integer range 0 to  2**APC_SCALE_WIDTH-1 := 0;
    -- signal limit_low_apc, limit_high_apc : signed (i_full_range'range);
    signal apc_scaled_down : signed (OUTPUT_WIDTH-1 downto 0):= (others=>'0');
    signal o_output_scaled_reg : signed(o_output_scaled'range):= (others=>'0');
	
BEGIN


    apc_scale_int <= to_integer(unsigned(i_apc_scale_idx));


    apc_scaled_down(apc_scaled_down'high) <= i_full_range(i_full_range'high) ;

    process(apc_scale_int, i_full_range)
    begin
        case apc_scale_int is
            when 0 =>                 
                apc_scaled_down(apc_scaled_down'high - 1 downto apc_scaled_down'high - 1)           <= i_full_range(0 downto 0); 
                apc_scaled_down(apc_scaled_down'high - 2 downto 0)  <= (others=>'0');
            when 1 =>                 
                apc_scaled_down(apc_scaled_down'high - 1 downto apc_scaled_down'high - 2)           <= i_full_range(1 downto 0); 
                apc_scaled_down(apc_scaled_down'high - 3 downto 0)  <= (others=>'0');
            when 2 =>                 
                apc_scaled_down(apc_scaled_down'high - 1 downto apc_scaled_down'high - 3)           <= i_full_range(2 downto 0); 
                apc_scaled_down(apc_scaled_down'high - 4 downto 0)  <= (others=>'0');
            when 3 =>                 
                apc_scaled_down(apc_scaled_down'high - 1 downto apc_scaled_down'high - 4)           <= i_full_range(3 downto 0); 
                apc_scaled_down(apc_scaled_down'high - 5 downto 0)  <= (others=>'0');
            when 4 =>                 
                apc_scaled_down(apc_scaled_down'high - 1 downto apc_scaled_down'high - 5)           <= i_full_range(4 downto 0); 
                apc_scaled_down(apc_scaled_down'high - 6 downto 0)  <= (others=>'0');
            when 5 =>                 
                apc_scaled_down(apc_scaled_down'high - 1 downto apc_scaled_down'high - 6)           <= i_full_range(5 downto 0); 
                apc_scaled_down(apc_scaled_down'high - 7 downto 0)  <= (others=>'0');
            when 6 => 
                apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(6 downto 0); 
            when 7 => 
                apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(7 downto 1); 
            when 8 => 
                apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(8 downto 2); 
            when 9 => 
                apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(9 downto 3); 
            when 10 => 
                apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(10 downto 4); 
            when 11 => 
                apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(11 downto 5); 
            when 12 => 
                apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(12 downto 6); 
            when others =>
               apc_scaled_down(apc_scaled_down'high - 1 downto 0)   <= i_full_range(apc_scaled_down'high - 1 downto 0);         
        end case;
    end process;


   -- saturated counter
    process(clk)
    begin
        if rising_edge(clk) then            
            if (en='1') then                
                o_output_scaled_reg <= apc_scaled_down;
            end if;
        end if;
    end process;

    o_output_scaled <= o_output_scaled_reg;
    
END Behavioral;



    
