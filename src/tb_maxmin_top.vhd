-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 29.10.2021 10:19:46 UTC

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.maxmin_net_pkg.all;

entity tb_maxmin_top is
end tb_maxmin_top;

architecture tb of tb_maxmin_top is

    component maxmin_top
        PORT
        (
            -- Inputs
            clk     : IN std_logic; 
            i_en    : IN std_logic;        

            -- X input		
            i_x0_data    : 	in std_logic_vector(4 downto 0);
            i_x1_data    : 	in std_logic_vector(4 downto 0);
            i_x2_data    : 	in std_logic_vector(4 downto 0);
            i_x3_data    : 	in std_logic_vector(4 downto 0);

            --Outputs
            o_neuron0_out : out std_logic_vector(8 downto 0);
            o_neuron1_out : out std_logic_vector(8 downto 0);
            o_neuron2_out : out std_logic_vector(8 downto 0);
            o_done        : out std_logic
        );
    end component;

    signal clk       : std_logic;
    signal i_en      : std_logic;
    signal i_x0_data  : std_logic_vector(4 downto 0);
    signal i_x1_data  : std_logic_vector(4 downto 0);
    signal i_x2_data  : std_logic_vector(4 downto 0);
    signal i_x3_data  : std_logic_vector(4 downto 0);
    signal o_neuron0_out : std_logic_vector(8 downto 0);
    signal o_neuron1_out : std_logic_vector(8 downto 0);
    signal o_neuron2_out : std_logic_vector(8 downto 0);
    signal o_done        : std_logic;

    constant TbPeriod : time := 20 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : maxmin_top
    port map (clk, i_en, i_x0_data, i_x1_data, i_x2_data, i_x3_data,
              o_neuron0_out, o_neuron1_out, o_neuron2_out, o_done);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';
    clk <= TbClock;

    stimuli : process
    begin
        -- RESET
        i_en <= '0';
        i_x0_data <= std_logic_vector(to_signed(0, i_x0_data'length));
        i_x1_data <= std_logic_vector(to_signed(0, i_x0_data'length));
        i_x2_data <= std_logic_vector(to_signed(0, i_x0_data'length));
        i_x3_data <= std_logic_vector(to_signed(0, i_x0_data'length));
        wait for 2 * TbPeriod;
       
        -- enable inference and wait results
        i_en <= '1';

        wait until o_done = '1';       
        wait;        
    end process;

end tb;
