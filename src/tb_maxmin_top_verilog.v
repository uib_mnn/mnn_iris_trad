
`timescale 1 ns/10 ps  // time-unit = 1 ns, precision = 10 ps

module tb_maxmin_top();
    
    localparam T = 20;

 
    reg clk;
    reg i_en;
    reg [4:0] i_x0_data;
    reg [4:0] i_x1_data;
    reg [4:0] i_x2_data;
    reg [4:0] i_x3_data;
    wire [8:0] o_neuron0_out;
    wire [8:0] o_neuron1_out;
    wire [8:0] o_neuron2_out;
    wire o_done;

    //	event do_dump;

    maxmin_top dut (
     .clk (clk),
     .i_en (i_en),
     .i_x0_data (i_x0_data),
     .i_x1_data (i_x1_data),
     .i_x2_data (i_x2_data),
     .i_x3_data (i_x3_data),
     .o_neuron0_out (o_neuron0_out),
     .o_neuron1_out (o_neuron1_out),
     .o_neuron2_out (o_neuron2_out),
     .o_done (o_done)
    );

    
    // clock generation
    initial begin
        clk = 1'b0;
        forever #(T/2) clk = ~clk;
    end

    // signal stimulus
    initial begin
        i_en = 1'b0;
        i_x0_data = 9'h000;
        i_x1_data = 9'h000;
        i_x2_data = 9'h000;
        i_x3_data = 9'h1FF;
        #(2*T);
        i_en = 1'b1;        
    end

endmodule
