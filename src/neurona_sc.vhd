-- autogenerate, neurona_sc v1
-- UNIVERSITAT DE LES ILLES BALEARS, PHYSICS DEPT., NOVEMBER 2018, JOSEP L. ROSSELLO
-- NEURONA ESTOCASTICA

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.maxmin_net_pkg.all;
USE work.MyTypes.all;

---------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 0.1 			Rossello            12/12/2018          Initial
-- 0.2 			CCFF				18/12/2018			compatibilidad red
-- 0.3			CCFF				7/01/2019			cambio entradas no potencias de 2
-- 0.4			CCFF				11/01/2019			Codigo mas facil de entender
-- 0.5 			CCFF 				8/3/2019			Relu con Or, scale up
-- 0.6			CCFF				14/3/2019			Function off, comentarios, apc saturado
-- 2.0			CCFF									My APC added.
-- 2.1													APC v2.1
-- 2.3			CCFF				13/9/2019			apc 2.3, modular
-- 2.4			CCFF				10/10/2019			neurona con apc_scale variable
-- 2.5          CCFF            	30/3/2020           Optimizacion de speed seleccionable
--=============================================================

ENTITY neurona_sc IS
    generic (				
        NEURON_N_INPUTS: integer:=29;		
        NEURON_F_TRANSFER : integer := 1;
          NEURON_INTG_PRD_CLK : integer := 256-1;
        NEURON_APC_MAX : integer := 7907;
        NEURON_APC_SCALE_WIDTH : integer := 8
        -- NEURON_APC_SCALE : integer := 1
    );
    PORT
    (
        -- Inputs
        clock : IN STD_LOGIC; 
        reset : IN STD_LOGIC;
        
        portIn_apc_scale_idx : IN std_logic_vector(NEURON_APC_SCALE_WIDTH-1 downto 0);
        portIn_bs_tick : 		IN STD_LOGIC; 			
        portIn_lfsr : 		IN STD_LOGIC_VECTOR (log2ceil(NEURON_INTG_PRD_CLK)-1 DOWNTO 0);
        portIn_x_sc : 		IN STD_LOGIC_VECTOR (0 to NEURON_N_INPUTS-1);
        portIn_w_sc : 		IN STD_LOGIC_VECTOR (0 to NEURON_N_INPUTS); -- un peso m�s que la portIn_x_sc. El LSB es el bias.
        portIn_0_sc : 		in std_logic;
        
        
        --Outputs
        portOut_y_sc : 		OUT STD_LOGIC;	
        portOut_apc_full : 		out signed(log2ceil(NEURON_N_INPUTS+2) + log2ceil(NEURON_INTG_PRD_CLK) downto 0);
        portOut_apc_scaled :		out signed(log2ceil(NEURON_INTG_PRD_CLK)-1 DOWNTO 0)  
    );
    
END neurona_sc;

ARCHITECTURE arch OF neurona_sc IS
    --------------------------------------	
    -- CONSTANTES 
    --------------------------------------
    -- para apc
    constant APC_N_INPUTS : integer := NEURON_N_INPUTS+1;	
    -- constant APC_N_OUTPUTS :  INTEGER := PKG_BIT_PRECISSION;
    constant APC_N_OUTPUTS :  integer := log2ceil(NEURON_INTG_PRD_CLK); 
    --------------------------------------	
    -- SE�ALES
    --------------------------------------
    -- se�ales para multiplicador
    SIGNAL in_n_bias : STD_LOGIC_VECTOR (0 to NEURON_N_INPUTS):= (others=>'0');
    SIGNAL mul_sc : STD_LOGIC_VECTOR (NEURON_N_INPUTS DOWNTO 0):= (others=>'0');
        
    -- Se�ales para APC
    SIGNAL reset_apc: STD_LOGIC:='0';
    signal apc_out_sc : std_logic:='0';
    -- signal apc_out_scaled :SIGNED (PKG_BIT_PRECISSION-1 DOWNTO 0); 
    signal apc_out_scaled : signed(log2ceil(NEURON_INTG_PRD_CLK)-1 DOWNTO 0):= (others=>'0');

    -- se�ales para p2b salida
    SIGNAL lfsr_signed: SIGNED (PKG_BIT_PRECISSION-1 DOWNTO 0):= (others=>'0'); 
    SIGNAL act_fun: SIGNED (PKG_BIT_PRECISSION-1 DOWNTO 0):= (others=>'0'); 

    signal p2b_x0_bin : std_logic_vector(7 downto 0):= (others=>'0');

    component apc IS 
    GENERIC(
        N_INPUTS : INTEGER := 4;
        INTG_PRD_CLK : INTEGER := 16;
        N_OUTPUTS :  INTEGER := 6 ;
        APC_MAX  :  INTEGER := 12306 ;
        APC_SCALE_WIDTH : INTEGER := 8          
    );
    PORT(
        -- inputs
        clk: in  std_logic;
        i_get_now_strobe: in  std_logic;

        i_input: in std_logic_vector(N_INPUTS-1 downto 0); 
        -- apc scale
         
        i_apc_scale_idx : in std_logic_vector(APC_SCALE_WIDTH-1 downto 0);
        -- outputs
        o_apc_out: out signed (log2ceil(N_INPUTS+1) + log2ceil(INTG_PRD_CLK) downto 0);
        o_apc_out_scaled: out signed (N_OUTPUTS-1 downto 0)						
        );
    END component;

BEGIN
    --------------------------------------------------------------------
    -- MULTIPLICACION
    --------------------------------------------------------------------
        -- el vector de entrada se le debe agregar el termino del bias.
        -- in_n_bias <= portIn_x_sc & '1'; 	
        in_n_bias(0 to NEURON_N_INPUTS-1) <= portIn_x_sc; -- para bias '1'
        in_n_bias(NEURON_N_INPUTS) <= '1';
        -- Multiplicaciones:  mul_sc = in_n_bias * pesos
        conversors: FOR index IN 0 TO NEURON_N_INPUTS GENERATE 
            mul_sc(index)<=in_n_bias(index) XNOR portIn_w_sc(index);
        END GENERATE;

    --------------------------------------------------------------------
    -- APC
    --------------------------------------------------------------------
        apc_inst : apc
        generic map(N_INPUTS => APC_N_INPUTS, 
                    INTG_PRD_CLK => NEURON_INTG_PRD_CLK, 
                    N_OUTPUTS => APC_N_OUTPUTS, 
                    APC_MAX => NEURON_APC_MAX, 
                    APC_SCALE_WIDTH => NEURON_APC_SCALE_WIDTH)
        port map (clock,reset_apc, mul_sc, portIn_apc_scale_idx, portOut_apc_full, apc_out_scaled);

        -- Se�al para reiniciar el apc. 
        reset_apc <= portIn_bs_tick or reset;
        

        
    --------------------------------------------------------------------
    -- Salida de neurona en funcion de NEURON_F_TRANSFER
    --------------------------------------------------------------------	
        lfsr_signed <= signed(portIn_lfsr(PKG_BIT_PRECISSION-1 downto 0));
       
        -- Relu sc
        GEN_RELU: if NEURON_F_TRANSFER = PKG_MLP_FA_RELU generate	
            portOut_apc_scaled <= 	apc_out_scaled;
            -- stochastic relu
            apc_out_sc <= '1' when apc_out_scaled >= lfsr_signed else '0'; 			
            portOut_y_sc <= portIn_0_sc or apc_out_sc;-- ReLu stochastic.
        end generate GEN_RELU;

        -- no function, linear	
        GEN_NO_FUNCTION: if NEURON_F_TRANSFER = PKG_MLP_FA_OFF generate
            portOut_apc_scaled <= 	apc_out_scaled;
            portOut_y_sc <= '0'; -- NO SE USA
        end generate GEN_NO_FUNCTION;		

END arch;
