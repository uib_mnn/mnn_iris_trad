# MNN Iris Trad
Este proyecto se ha creado para simular la red MNN de 4 neuronas para IRIS dataset utilizando sumadores _tradicionales_.

Dentro del repo estan todos los ficheros fuentes en `.vhd` salvo el testbench que está escrito en `verilog` : `tb_maxmin_top_verilog.v`. 

En la subcarpeta `vcd` he puesto la última simulación que realicé usando el `ModelSim` para comprobar con el `vcd` generado por la herramienta de sintesis de `CADENCE`. 

Además de esto, se ha agregado el comando para la herramienta `XCELIUM` para simular el RTL en `CADENCE` (ver [XCELIUM_command.txt](src/XCELIUM_command.txt)).

# Métricas
- Accuracy en Hardware (using SC): `96%`, LFSR 2 semilla: `26`

# Diagrama de diseño top 

![Diagram](_Doc/maxmin_top.svg "Diagram")
## Ports

| Port name     | Direction | Type                         | Description |
| ------------- | --------- | ---------------------------- | ----------- |
| clk           | in        | std_logic                    |             |
| i_en          | in        | std_logic                    |             |
| i_x0_data     | in        | std_logic_vector(4 downto 0) |             |
| i_x1_data     | in        | std_logic_vector(4 downto 0) |             |
| i_x2_data     | in        | std_logic_vector(4 downto 0) |             |
| i_x3_data     | in        | std_logic_vector(4 downto 0) |             |
| o_neuron0_out | out       | std_logic_vector(8 downto 0) |             |
| o_neuron1_out | out       | std_logic_vector(8 downto 0) |             |
| o_neuron2_out | out       | std_logic_vector(8 downto 0) |             |
| o_done        | out       | std_logic                    |             |



